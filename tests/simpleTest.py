import allure

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

# config_webdriver
# driver = webdriver.Chrome(ChromeDriverManager().install())
#driver.maximize_window()

# Uncomment the code below for config_webdriver_with_headless_option, for remote execution
s=Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=s)
driver.maximize_window()

# Load a page
driver.get("https://the-internet.herokuapp.com/context_menu")

content = driver.find_element(By.ID, 'content').text


class TestPage:

    @allure.title("Home page - contains text")
    @allure.description(
        "Test should SUCCESS! Check if home page contains  - Right-click in the box below to see one called "
        "'the-internet'")
    def test_verify_page_contains_text(self):
        assert content.__contains__("Right-click in the box below to see one called 'the-internet'")

    @allure.title("Home page - not contains text")
    @allure.description("Test should FAIL! Check if home page not contains  - Alibaba")
    def test_verify_page_not_contains_text(self):
        assert content.__contains__("Alibaba"), "Alibaba was not found on Page!!!"


driver.close()
